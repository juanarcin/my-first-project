import React from 'react'
import { Link } from 'react-router-dom'

import '../SCSS/Navigation.scss'

// The Header creates links that can be used to navigate
// between routes.
const Navigation = () => (
  <header>
    <nav>
      <ul>
        <li><Link to='/'>Home</Link></li>
        <li><Link to='/page-one'>Page One</Link></li>
        <li><Link to='/page-two'>Page Two</Link></li>
      </ul>
    </nav>
  </header>
)

export default Navigation