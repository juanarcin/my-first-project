const initialState = {
	test: 'test'

}

const reducer = (state = initialState, action) => {
	if(action.type === 'SOME_ACTION'){
		return {
			...state,
			test: action.payload
		}
	}
  return state;
};


export default reducer;
