import React, { Component } from 'react';
import '../SCSS/Users.scss';

class ImportData extends Component {
  constructor(props) {
		super(props);
		this.state = {
			pictures: [],
		}
	}

	componentDidMount(){
		const url = 'https://randomuser.me/api/?results=500';
		fetch(url)
		.then(results => {
			return results.json();
		})
		.then(data => {
			let pictures = data.results.map((pic, index) => {
				const name=`${pic.name.first} ${pic.name.last}`
				return(
					<div key={index} className="user">
						<img src={pic.picture.medium}  alt={name}/>
					</div>
				)
			})
			this.setState({ pictures: pictures});
		})
	}

	render(){
		return (
			<div id="background">
				{this.state.pictures}
			</div>
		)
	}
}

export default ImportData;