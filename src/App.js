import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'

import ImportData from './Components/Data.js';
import Navigation from './Components/Navigation';
import Home       from './Components/Pages/Home';
import PageOne    from './Components/Pages/PageOne';
import PageTwo    from './Components/Pages/PageTwo';


class App extends Component {
  render() {
    return (
      <div className="App">
        <ImportData />
        <div id="content">
          <Navigation />
          <div className="row">
            <Switch>
              <Route exact path='/' component={Home}/>
              <Route path='/page-one' component={PageOne}/>
              <Route path='/page-two' component={PageTwo}/>
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

export default App;

